#!/usr/bin/env python3
"""Main class implementing high-order functions and the interface with the user
for the poc_cardano application.

Attributes:
    ARGS (:obj:`argparse.Namespace`): Parsed arguments.
    CONFIG_PATH (:obj:`pathlib.Path`): Path of the config file.
    CONFIG (:obj:`dict`): Readed configuration file.

Todos:
    * Refactor all the code in main.
 """

import json
import os
import sys
from datetime import datetime
from pathlib import Path
from warnings import warn
from .config.arg_parser import PARSER
from .config.loader import read_conf
from .server.message_db import MessageSQL, PostgreWrapperAdmin, table_print

def postgre_wrapper_from_conf(config):
    """Return the postgreSQL wrapper from a given configuration file.

    Args:
        config (dict): Dictionary containing the configuration.

    Return:
        obj:`PostgreWrapperAdmin`
    """
    return PostgreWrapperAdmin(**config['server']['postgresql'])


def check_messages(messages_dir):
    """Check not readed messages in the message dir.

    Args:
        messages_dir (str or :obj:`pathlib.Path`): Path containing the server
            recieved messages.

    Returns:
        :obj:`list` of str containing the unread messages.
    """
    unr_lst = []
    for item in Path(messages_dir).glob('*.json'):
        try:
            with open(item, 'r', encoding='utf-8') as infile:
                msg_json = json.load(infile)
            if not msg_json['retrieved']:
                unr_lst.append(msg_json)
        except json.decoder.JSONDecodeError:
            continue
    return unr_lst


def decrypt_metadata(metadata, password, img_path):
    """Decodes and the decrypt the message stored in the metadata recieved from
    a GraphQL query.

    Args:
        metadata (dict): Dict containing the metadata.
        password (str): Password of the encrypted image.
        img_path (str or :obj:`pathlib.Path`): Path where the image will be
            stored.
    """
    from .crypto.utils import CryptImg
    img_path = Path(img_path)
    img_obj = CryptImg.from_dict(metadata)
    img_obj.decrypt(password)
    img_obj.to_image(img_path)


def encrypt_image(image_path, password, metadata_path):
    """Encrypt, encode and save an image in metadata json format to be uploaded
    to the Cardano blockchain.

    Args:
        image_path (str or :obj:`pathlib.Path`): Path of the image.
        password (str): Password that will be used to decrypt the image.
        metadata_path (str or :obj:`pathlib.Path`): Path where the metadata
            file will be saved.

    Returns:
        int describing the size of the generated json file.
    """
    from .crypto.utils import CryptImg
    image_path = Path(image_path)
    crypt_image = CryptImg.from_raw(image_path)
    crypt_image.encrypt(password)
    crypt_image.encrypted_to_json()
    crypt_image.to_json(metadata_path)
    return len(crypt_image.json)


def message_request_metadata(message, metadata_path,
                             graphql_url, graphql_query_file):
    """Given a recieved message, request the metadata to the graphql API.

    Args:
        message (dict): Message containing the transaction id (txid). See the
            `check_messages` functions for more information.
        metadata_path : ??? to implement.
        graphql_url (str): Url of the graphql server.
        graphql_query_file (str or :obj:`pathlib.Path`): File containing the
            query to the graphQL API.

    Returns:
        str containing the retrieved metadata
    """
    from .cardano.graphql_request import get_metadata
    metadata_path = Path(metadata_path)
    if isinstance(message, MessageSQL):
        txid = message.txid
    else:
        txid = message['data']['txid']
    metadata = get_metadata(txid, graphql_url,
                            graphql_query_file)
    # Repair file
    new_metadata = {'0': metadata['value']}
    return new_metadata


def metadata_upload(metadata_path, config, prompt=True):
    """Uploads the given metadata to the Cardano blockchain.

    Args:
        metadata_path: ??? to be implemented.
        config (dict): Dictionary containing the needed configuration. See the
            `config.loader.load_cofig` function for additional information.
        prompt (bool, optional): If True, promp the user about the transaction.
            Defaults to True.

    Returns:
     :obj:`SingleTransaction` containing the details of the transaction.
    """
    from .cardano.cli_wrapper import CardanoCli
    cli = CardanoCli.from_config(config)
    # Draft and calculate fee
    transaction = cli._metadata_draft_transaction()
    cli.run_transaction(transaction)
    fee = cli.run_calc_min_fee(transaction)
    cli._metadata_update_draft_transaction(transaction, fee)
    amount = cli.utxo_table.total_amount()
    final_bal = amount - fee
    if final_bal < 0:
        msg = "You do not have enough currency in your wallet to proceed!"
        raise ValueError(msg)
    if prompt:
        msg = f"""Transaction will cost {fee} lovelaces you have a total of
               {amount} lovelaces in your account. Your final balance will be
               {final_bal}. \n Do you want to proceed? [y]es/[n]o\n"""
        select = False
        while not select:
            select = input(msg)
            print('')
            if select in ['y', 'Y']:
                break
            if select in ['n', 'N']:
                print('Transaction cancelled')
                sys.exit(1)
            print('Prease answer [y]es or [n]o\n')
            select = False
    cli.run_transaction(transaction)
    cli.run_sign(transaction)
    cli.run_submit(transaction)
    return transaction


def pretty_print_messages(messages):
    """Format a print a list of messages.

    Args:
        messages (:obj:`list` of str): List containing the messages that will
            be printed.
    """
    for idx, msg in enumerate(messages):
        print(f"[{idx}] {msg['data']['address']}{msg['recieved']}")


def update_config(config_def, config_new):
    """Override a default configuration dictionary with a new set of values.

    Args:
        config_def (dict): Default dictionary that will be updated.
        config_new (dict): New dictionary with the values that will be updated

    Returns:
        Updated configuration.
    """
    for key, item in config_new.items():
        if key not in config_def:
            warn(f"Missing {key} in the default configuration, skipping...")
        if isinstance(item, dict):
            update_config(config_def[key], item)
        else:
            config_def[key] = item
    return config_def


def run_query(config, txid=None, keywords=None, note=None, date=None,
              clientid=None, unretrieved=None, retrieved=None):
    """Query the SQL database to recieve the matching messages.

    Args:
        config (dict): Dictionary containing the needed configuration. See the
                `config.loader.load_cofig` function for additional information.
    """
    psql = postgre_wrapper_from_conf(config)
    all_messages = map(MessageSQL.from_query, psql.query_all())
    fts = []
    if txid:
        fts.append(lambda x: x.txid == txid)
    if keywords:
        fts.append(lambda x: x.keywords and set(keywords).issubset(set(x.keywords)))
    if note:
        fts.append(lambda x: x.note and note in x.note)
    if date:
        s_date, e_date = map(datetime.fromisoformat, date)
        fts.append(lambda x: s_date <= x.date_sent <= e_date)
    if clientid:
        fts.append(lambda x: x.clientid == clientid)
    if unretrieved:
        fts.append(lambda x: x.date_retrieved is None)
    if retrieved:
        s_date, e_date = map(datetime.fromisoformat, retrieved)
        fts.append(lambda x: x.date_retrieved is not None
                   and s_date <= x.date_retrieved <= e_date)
    store = [item for item in all_messages if all(ft(item) for ft in fts)]
    return store


def run_retrieve(config, password=None):
    """Query, download, store, decrypt and encode an image stored in the Cardano
    blockchain selecting a message.

    Args:
        config (dict): Dictionary containing the needed configuration. See the
                `config.loader.load_cofig` function for additional information.
        password (str, optional): Password that will be used to encrypt the
            image. Defaults to None.
    """
    graphql_url = config['graphql']['url']
    graphql_query = config['graphql']['query']
    metadata_path = config['cardano']['paths']['metadata']
    message_path = config['server']['m_path']
    my_msg = check_messages(message_path)
    pretty_print_messages(my_msg)
    select = len(my_msg)
    while select >= len(my_msg) or select < 0:
        msg = "Select a messagge:\n"
        select = int(input(msg))
    password = input('Password: ')
    sel_msg = my_msg[select]
    metadata = message_request_metadata(sel_msg, metadata_path,
                                        graphql_url, graphql_query)
    txid = sel_msg['data']['txid']
    final_path = Path(config['images']['path']) / txid
    decrypt_metadata(metadata, password, final_path)


def run_retrieve_from_query(config, msg_list, password=None):
    """Query, download, store, decrypt and encode an image stored in the Cardano
    blockchain selecting a message.

    Args:
        config (dict): Dictionary containing the needed configuration. See the
                `config.loader.load_cofig` function for additional information.
        msg_list (list): List of retrieved messages.
        password (str, optional): Password that will be used to encrypt the
            image. Defaults to None.

    Returns:

    String containing the final path of the downloaded image.
    """
    graphql_url = config['graphql']['url']
    graphql_query = config['graphql']['query']
    metadata_path = config['cardano']['paths']['metadata']
    select = len(msg_list)
    while select >= len(msg_list) or select < 0:
        msg = "Select a messagge:\n"
        select = int(input(msg))
    password = input('Password: ')
    sel_msg = msg_list[select]
    metadata = message_request_metadata(sel_msg, metadata_path,
                                        graphql_url, graphql_query)
    txid = sel_msg.txid
    final_path = Path(config['images']['path']) / txid
    if final_path.is_file():
        print("Warining! File exists. overwrite?")
        readed = str(input("[y/[N]] "))
        select = (readed in ["Y", "y"])
        if not select:
            return None
    decrypt_metadata(metadata, password, final_path)
    psql = postgre_wrapper_from_conf(config)
    psql.entry_update_date_retrieved_by_uuid(sel_msg.uuid, datetime.now())
    return final_path


def run_send(host, port, image_path, config, password=None, keywords=None, note=None):
    """Encrypt, encode, transform to metadata and upload an image to the Cardano
    blockchain.

    Args:
        host (str): Ip address of the target server where the message will be
            sent (not the image).
        port (int): Port of the target server of the server where the message
            will be sent (not the image)
        config (dict): Dictionary containing the needed configuration. See the
                `config.loader.load_cofig` function for additional information.
        password (str, optional): Password that will be used to encrypt the
            image. Defaults to None.

    Returns:
        :obj:`SimpleTransaction` with the data of the transaction.
    """
    print(f'Encrypting {image_path}')
    if not password:
        password = input('Password: ')
    encrypt_image(image_path, password,
                  config['cardano']['paths']['metadata_tmp'])
    print('Uploading transaction')
    transaction = metadata_upload(config['cardano']['paths']['metadata'],
                                  config)
    print(f'Transaction succesfully loaded with txid {transaction.txid}')
    send_message(host, port, transaction.txid, keywords, note)
    return transaction


def run_server(config):
    """Run the server using a given configuration.

    Args:
        config (dict): Dictionary containing the needed configuration. See the
                `config.loader.load_cofig` function for additional information.
    """
    from .server.msg_server import RecServer
    serv_obj = RecServer(**config['server'])
    serv_obj.listen()
    serv_obj.run()


def send_message(host, port, txid, keywords=None, note=None):
    """Send a message containing a transaction id (txid) to a server listening.

    Args:
        host (str): Ip address of the target server.
        port (int): Port of the target server.
        txid (str): Transaction id (txid) that will be sent.
    """
    from .server.msg_client import RecClient, txid_message
    client_obj = RecClient(host=host, port=port)
    client_obj.connect()
    message_obj = txid_message(txid, (host, port), keywords=keywords, note=note)
    client_obj.buff_message(message_obj)
    client_obj.send_message()


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ARGS = PARSER.parse_args()
CONFIG_RC = Path.home() / Path('.poc_cardanorc')
CONFIG = read_conf(ROOT_DIR / Path('./config/config.yaml'))
if CONFIG_RC.is_file():
    update_config(CONFIG, read_conf(CONFIG_RC))
if ARGS.config and Path(ARGS.config).is_file():
    update_config(CONFIG, read_conf(Path(ARGS.config)))

if ARGS.commands == 'server':
    run_server(CONFIG)
if ARGS.commands == 'retrieve':
    run_retrieve(CONFIG, ARGS.password)
if ARGS.commands == 'send':
    run_send(ARGS.host, ARGS.port, ARGS.image, CONFIG, ARGS.password,
             ARGS.keywords, ARGS.note)
if ARGS.commands == 'query':
    MSGS = run_query(CONFIG, ARGS.txid, ARGS.keywords, ARGS.note, ARGS.date,
                     ARGS.clientid, ARGS.unretrieved, ARGS.retrieved_date)
    if ARGS.simple:
        fields = ["#", "clientid", "date_sent",
                  "date_retrieved", "keywords", "note"]
    else:
        fields = ARGS.fields
    table_print(MSGS, numbered=True, fields=fields)
    if ARGS.retrieve:
        file_location = run_retrieve_from_query(CONFIG, MSGS, password=ARGS.password)
        if file_location:
            print(f"File saved in {file_location}")
        else:
            print("File not retrieved.")
