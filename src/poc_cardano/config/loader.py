#!/usr/bin/env python3
"""Obtain configuration from the give yaml file.

Attributes:
    DEFAULT_PATH = Default path of the configuration file.
"""

from warnings import warn
from pathlib import Path
import yaml

DEFAULT_PATH = Path("./config/config.yaml")


def read_conf(config_path=None):
    """Reads and parses the given yaml file.

    Args:
        config_path (str or :obj:`pathlib.Path`, optional): Location of the
            configuration file. Defaults to the DEFAULT_PATH attribute.

    Returns:
        dict containing the keys and values parsed from the specified file.
    """
    if not config_path:
        msg = 'No config path specified, looking for {}'.format(DEFAULT_PATH)
        warn(msg)
        config_path = DEFAULT_PATH
    with open(config_path, 'r', encoding='utf-8') as infile:
        config = yaml.load(infile, Loader=yaml.FullLoader)
    if 'images' in config and 'path' in config['images']:
        config['images']['path'] = Path(config['images']['path']).expanduser()
    if 'server' in config and 'm_path' in config['server']:
        config['server']['m_path'] = Path(config['server']['m_path']).expanduser()
    if 'cardano' in config and 'paths' in config['cardano']:
        for key, item in config['cardano']['paths'].items():
            config['cardano']['paths'][key] = Path(item).expanduser()
    return config
