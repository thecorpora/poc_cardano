#!/usr/bin/env python3
"""Argument parser module."""

import argparse

PARSER = argparse.ArgumentParser()
PARSER.add_argument("-c", "--config", type=str, default=None,
                    help="""Location of the config file, if not specified""")
SUBPARSERS = PARSER.add_subparsers(help='Action to execute', dest='commands')

SERVER_PARSER = SUBPARSERS.add_parser('server',
                                      help="""Start msg server""")

SEND_PARSER = SUBPARSERS.add_parser('send',
                                    help="""Upload metadata to cardano and send
                                            a message""")
SEND_PARSER.add_argument("-ha", "--host", type=str, default="127.0.0.1",
                         help="""Server address, defaults to 127.0.0.1""")
SEND_PARSER.add_argument("-p", "--port", type=int, default=64560,
                         help="""Servert port, defaults to 64560""")
SEND_PARSER.add_argument("-i", "--image", type=str, default=None,
                         help="""Raw image path that will be added to the
                                 metadata""")
SEND_PARSER.add_argument("-m", "--message", type=str, default=None,
                         help="""Message that will be added to the metadata""")
SEND_PARSER.add_argument("-pwd", "--password", type=str, default=None,
                         help="""Password used to encrypt the file""")
SEND_PARSER.add_argument("-kw", "--keywords", nargs='+', default=None,
                         help="""Keywords that will be in the message""")
SEND_PARSER.add_argument("-n", "--note", type=str, default=None,
                         help="""Note that will be added to the message""")


RETR_PARSER = SUBPARSERS.add_parser('retrieve',
                                    help="""Retrieve metadata from cardano
                                            blockchain using a message""")
RETR_PARSER.add_argument("-pwd", "--password", type=str, default=None,
                         help="""Password used to decrypt the file""")

SQL_PARSER = SUBPARSERS.add_parser('query',
                                    help="""Perform a query from the sql
                                    database""")

SQL_PARSER.add_argument("-tx", "--txid", type=str, default=None,
                         help="""Query txid""")
SQL_PARSER.add_argument("-kw", "--keywords", type=str, nargs='+', default=None,
                         help="""Query Keywords""")
SQL_PARSER.add_argument("-n", "--note", type=str, default=None,
                         help="""Query Note""")
SQL_PARSER.add_argument("-d", "--date", type=str, nargs=2, default=None,
                        help="""Query sent date range in ISO format
                        (yyyy-mm-dd).""")
SQL_PARSER.add_argument("-c", "--clientid", type=str, default=None,
                        help="""Query sender.""")
SQL_PARSER.add_argument("-u", "--unretrieved", action="store_true",
                        help="""Query unretrieved.""")
SQL_PARSER.add_argument("-rd", "--retrieved_date", type=str, nargs=2, default=None,
                        help="""Query retrieved date range in ISO format
                        (yyyy-mm-dd).""")
SQL_PARSER.add_argument("-s", "--simple", action="store_true",
                        help="""Return a simplified table, showing less
                            information.""")
SQL_PARSER.add_argument("-f", "--fields", type=str, nargs='+', default=None,
                         help="""Show only the given fields.""")
SQL_PARSER.add_argument("-r", "--retrieve", action="store_true",
                         help="""Retrieve the image after the query.""")
SQL_PARSER.add_argument("-pwd", "--password", type=str, default=None,
                         help="""Password used to encrypt the file. Only used if
                         retrieve is specified.""")
