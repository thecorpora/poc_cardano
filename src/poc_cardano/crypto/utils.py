#!/usr/bin/env python3
"""Module to encrypt, decrypt and encode images in the required metadata
format to upload it to the cardano network.#!/usr/bin/env python

Todos:
    * Add error handling
"""

from pathlib import Path
import re
import base64
import json
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256


def read_bytes(filename):
    """Read a file in bytes.

    Args:
        filename (str or pathlib.Path): Location of the file to read.

    Returns:
        bytestring with the contents of the file.
    """
    with open(filename, 'rb') as infile:
        b_file = infile.read()
    return b_file


def write_bytes(filename, data):
    """Write a file in bytes.

    Args:
        filename (str or pathlib.Path): Location of the file to read.
        data (bytestring): Bytestring that will be written
    """
    with open(filename, 'wb') as outfile:
        outfile.write(data)


def encode_n_serialize(bytes_image):
    """Encode an image bytestring using the base64.b64encode.

    Args:
        bytes_image (bytestring): Bytestring that will be encoded.
    """
    b64_enc = base64.b64encode(bytes_image)
    serialized = re.findall('.{1,64}', b64_enc.decode('utf-8'))
    return serialized


def deserialize_n_decode(json_dict):
    """Given a json dict encoding an image obtained from the `to_json`
    function, decode the image inside the json using the base64.b64encode.

    Args:
        json_dict (:obj:`dict`): Dict containing the encoded image.

    Returns:
        bytestring containing the decoded image.
    """
    encoded = ''.join(json_dict['0']['data'])
    decoded = base64.b64decode(encoded)
    return decoded


def to_json(bytes_image, ext=''):
    """Encode and wrap into a json the given image bytestring.

    Args:
        bytes_image (bytestring): Bytestring image to convert.
        ext (str, optional): Extension of the image. Defaults to ''

    Returns:
        str in json format containing the encoded image with the given
        structure: {'0': {'ext': extension, 'data': [...]}}
    """
    encoded = encode_n_serialize(bytes_image)
    out_dict = {'0': {'ext': ext, 'data': encoded}}
    t_json = json.dumps(out_dict)
    return t_json


def get_SHA(password):
    """Given a password, get the `SHA256` hash digest.

    Args:
        password (str): Used password.

    Returns:
        bytestring with the hash digest, computed over the data processed so
        far.
    """
    sha = SHA256.new(password.encode())
    return sha


def gen_SHA_iv(password):
    """Given a password, get the `SHA256` hash digest and the iv.

    Args:
        password (str): Used password.

    Returns:
        bytestring with the hash digest, computed over the data processed so
        far and the iv code.
    """
    sha_code = get_SHA(password).digest()
    iv_code = sha_code.ljust(16)[:16]
    return sha_code, iv_code


def encrypt_image(image_byt, key, iv_code):
    """Given an image bytestring and a SHA digest and its associated iv code,
    encrypt the image using `AES`.

    Args:
        image_byt (bytestring): Image that will be encrypted.
        key (bytestring): SHA digest of a given password.
        iv_code (bytestring): iv associated with the key bytestring.

    Returns:
        bytestring containing the encrypted image with the given SHA digest and
        iv.
    """
    cipher = AES.new(key, AES.MODE_CFB, iv_code)
    encoded = cipher.encrypt(image_byt)

    return encoded


def decrypt_image(image_enc, key, iv_code):
    """Given an encrypted image bytestring and a SHA digest and its associated
    iv code, decrypt the image using `AES`.

    Args:
        image_byt (bytestring): Encrypted image that will be decrypted.
        key (bytestring): SHA digest of a given password.
        iv_code (bytestring): iv associated with the key bytestring.

    Returns:
        bytestring containing the decrypted image using the given SHA digest
        and iv.
    """
    cipher = AES.new(key, AES.MODE_CFB, iv_code)
    plain_data = cipher.decrypt(image_enc)

    return plain_data


def gen_RSA_pair():
    """NOT USED, future addition"""
    rsa_priv = RSA.generate(2048)
    rsa_pub = rsa_priv.publickey()

    with open('priv_key.pem', 'wb') as outfile:
        outfile.write(rsa_priv.export_key('PEM'))

    with open('pub_key.pem', 'wb') as outfile:
        outfile.write(rsa_pub.export_key('PEM'))


class CryptImg:
    """Image wrapper for encrypted and decrypted images.#!/usr/bin/env python

    Attributes:
        raw (bytestring): bytestring of the raw image.
        encrypted (bytestring): bytestring of the encrypted image.
        json (str): json string containing the encoded and encrypted image.
        ext (str): extension of the image

    Note:
        Do not build an object of this class with the init method. Use the
        `from_raw`, `from_encrypted` and `from_json` classmethods instead.
    """
    def __init__(self):
        self.raw = None
        self.encrypted = None
        self.json = None
        self.ext = None

    @classmethod
    def from_raw(cls, filename):
        """Create an :obj:`CryptImg` from an image.

        Args:
            filename (str or :obj:`pathlib.Path`): Path to the image.
        """
        n_image = cls()
        n_image._read_raw(filename)
        return n_image

    @classmethod
    def from_encrypted(cls, filename):
        """Create an :obj:`CryptImg` from an encrypted image.

        Args:
            filename (str or :obj:`pathlib.Path`): Path to the encrypted image.

        Returns:
            :obj:`CryptImg` created with the given data.
        """
        n_image = cls()
        n_image._read_encoded(filename)
        return n_image

    @classmethod
    def from_json(cls, filename):
        """Create an :obj:`CryptImg` from an structured json file.

        Args:
            filename (str or :obj:`pathlib.Path`): Path to the json file.

        Returns:
            :obj:`CryptImg` created with the given data.
        """
        with open(filename, 'r', encoding='utf-8') as infile:
            json_raw = infile.read()
        n_image = cls.from_jsons(json_raw)
        return n_image

    @classmethod
    def from_jsons(cls, json_s):
        """Create an :obj:`CryptImg` from an structured json string.

        Args:
            json_s (str): Json string containing the image.

        Returns:
            :obj:`CryptImg` created with the given data.
       """
        # 0 is the header required for Cardano
        json_data = json.loads(json_s)
        n_image = cls.from_dict(json_data)
        return n_image

    @classmethod
    def from_dict(cls, img_dict):
        """Create an :obj:`CryptImg` from an structured :obj:`dict`.

        Args:
            img_dict (:obj:`dict`): Structured dict containing the image.

        Returns:
            :obj:`CryptImg` created with the given data.
        """
        n_image = cls()
        n_image.json = img_dict
        n_image.ext = img_dict['0']['ext']
        n_image.encrypted = deserialize_n_decode(img_dict)
        return n_image

    def to_image(self, filename):
        """Save a raw image to the given file.

        Args:
            filename (str or :obj:`pathlib.Path`): Path in which the image
                will be stored.
        """
        if not self.raw:
            raise ValueError('Missing value in raw attribute')
        path = Path(filename)
        if not path.suffix and self.ext:
            path /= f'.{self.ext}'
        write_bytes(filename, self.raw)

    def to_json(self, filename):
        """Save a raw json with the encoded and encrypted image to the given
        file.

        Args:
            filename (str or :obj:`pathlib.Path`): Path in which the json
                will be stored.
        """
        if not self.json:
            raise ValueError('Missing value in json attribute')
        with open(filename, 'w', encoding='utf-8') as outfile:
            outfile.write(self.json)

    def _to_encrypted(self, filename):
        if not self.encrypted:
            raise ValueError('Missing value in encrypted attribute')
        write_bytes(filename, self.encrypted)

    def _read_raw(self, filename):
        extension = Path(filename).suffix
        if extension:
            # Delete period
            self.ext = extension[1:]

        with open(filename, 'rb') as infile:
            self.raw = infile.read()

    def _read_encoded(self, filename):
        self.encrypted = read_bytes(filename)

    def encrypted_to_json(self):
        """Encode and transform the image in the encrypted attribute to json.
        """
        self.json = to_json(self.encrypted)

    def encrypt(self, password):
        """Encrypt the image contained in the raw attribute with the given
        password.

        Args:
            password (str): Password that will be used to encrypt the file.
        """
        if not self.raw:
            raise ValueError('Missing value in raw attribute')

        sha_code, iv_code = gen_SHA_iv(password)
        self.encrypted = encrypt_image(self.raw, sha_code, iv_code)

    def decrypt(self, password):
        """Decrypt the image contained in the encrypt attribute with the given
        password.

        Args:
            password (str): Password that will be used to encrypt the file.
        """
        if not self.encrypted:
            raise ValueError('Missyng value in encrypted attribute')

        sha_code, iv_code = gen_SHA_iv(password)
        self.raw = decrypt_image(self.encrypted, sha_code, iv_code)
