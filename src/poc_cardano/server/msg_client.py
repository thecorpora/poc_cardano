#!/usr/bin/env python3
"""Client class to implement the client/server protocol.

Attributes:
    TimeZone (Timezone): Madrid default timezone to log the messages.

Todos:
    * Error handling.
    * Improve server implementation.
    * Add unique ids to the sent messages.
"""

import socket
import selectors
from datetime import datetime
import json
#from pytz import timezone
from .message import Message

#TimeZone = timezone('Europe/Madrid')


def txid_message(txid, address, keywords=None, note=None):
    """Send a message containing a transaction id.

    Args:
        txid (str): Transaction id that will be sent.
        address ((str, int)): Destination address of the server.
    """
    message = {'txid': txid,
               'address': '{}:{}'.format(*address),
               'created': datetime.now().isoformat()}
    message['keywords'] = keywords
    message['note'] = note
    json_message = json.dumps(message)
    return json_message


class RecClient:
    """Object implementing the client.

    Attributes:
        host (str): Ip address of the server.
        port (int): Port of the server.
        sel (:obj:`selectors.BaseSelector`): Selector to operate with the
            sockets.
        socket_type (str): Type of the socket that will be used to communicate
            with the server.

    Args:
        host (str, optional): Same as attribute. Defaults to None.
        port (int, optional): Same as attribute. Defaults to None
        sel (:obj:`selectors.BaseSelector`, optional): Same as attribute.
            Defaults to None. If None, an :obj:`selectors.BaseSelectors` will
            be created as the selector.
        socket_type (str, optional): Same as attribute. Defaults to None. If
            None, a `sockets.AF_INET` `socket.SOCK_STREAM` will be used.

    Raises:
        NotImplementedError: Wrong selector.
    """
    CONNID = 0

    def __init__(self, host=None, port=None, sel=None, socket_type=None):
        self.host = host
        self.port = port

        if sel:
            if isinstance(sel, selectors.BaseSelector):
                self.sel = sel
            else:
                msg = 'sel must be a valid selector object.'
                raise NotImplementedError(msg)
        else:
            self.sel = selectors.DefaultSelector()
        if socket_type:
            if isinstance(socket_type, socket.socket):
                self.socket = socket_type
            else:
                msg = 'socket_type must be a valid socket object.'
                raise NotImplementedError(msg)
        else:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.status = 'rest'

    def __repr__(self):
        return f'<RecClient: {self.host}:{self.port}>'

    def connect(self):
        """Connect to the server."""
        RecClient.CONNID += 1
        print('starting connection',
              RecClient.CONNID,
              f'to {self.host}:{self.port}')
        self.socket.setblocking(False)
        self.socket.connect_ex((self.host, self.port))

    def buff_message(self, message):
        """Create a :obj:`Message` with a arbitrary data and load the message in
        the :obj:`Message` buffer and register the message in the selector.

        Args:
            message (str): Data that will be loaded.

        Returns:
            :obj:`Message` created.

        """
        events = selectors.EVENT_WRITE
        data = Message(self.sel, self.socket, (self.host, self.port))
        data.generate_content_auto('text/json', message)
        data.write_buffer()
        self.sel.register(self.socket, events, data=data)
        return data

    def send_message(self):
        """Sends the :obj:`Message` stored in the selector to the server. Stops
        if KeyboardInterrupt. Closes the selector before terminate.
        """
        try:
            while True:
                events = self.sel.select(timeout=1)
                if events:
                    for key, mask in events:
                        key.data.send()
                if not self.sel.get_map():
                    break
        except KeyboardInterrupt:
            print("caught keyboard interrupt, exiting")
        finally:
            self.sel.close()
