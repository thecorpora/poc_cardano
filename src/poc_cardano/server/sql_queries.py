#!/usr/bin/env python3

"""This file contains a series of queries to execute by postgresql database
"""

# Message Table
MESSAGE_TABLE_CREATE = """
    CREATE TABLE IF NOT EXISTS message (
        uuid uuid PRIMARY KEY,
        txid text,
        clientid text,
        date_sent timestamp,
        date_retrieved timestamp,
        keywords text[],
        note text)
    """
MESSAGE_TABLE_DROP = """
    DROP TABLE IF EXISTS message
    """
MESSAGE_INSERT = """
    INSERT INTO message
        (uuid, txid, clientid,
        date_sent, date_retrieved,
        keywords, note)
    VALUES (%s, %s, %s, %s, %s, %s, %s)
    """
MESSAGE_UPDATE = """
    UPDATE message SET {field_target} = %s WHERE {field_select} = %s
"""

# Searches and filters
QUERY_KEYWORDS = """
    SELECT uuid, array_agg(keyword)
    FROM (
        SELECT uuid, unnest(keywords) AS keyword
        FROM message
    ) S
    WHERE keyword like '%%' || %s || '%%'
    GROUP BY uuid
    """
QUERY_GENERIC = """
    SELECT * FROM message WHERE {field} = ANY(%s)
"""
QUERY_ALL = """
    SELECT * FROM message
"""
QUERY_NOTE = """
    SELECT  uuid, note
    FROM (
        SELECT P.uuid
            ,ts_rank_cd(P.note::tsvector, to_tsquery(%s::text))
                AS score
            ,P.note
        FROM message as P
    ) S
    ORDER BY score DESC
"""
QUERY_DATE = """
    SELECT * FROM message
    WHERE {field} >= %s AND {field} < %s
"""
QUERY_UNRETRIEVED = """
    SELECT * FROM message WHERE date_retrieved IS NULL
"""
