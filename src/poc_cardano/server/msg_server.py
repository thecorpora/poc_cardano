#!/usr/bin/env python3
"""Module to import the server class for the server/client protocol.

Todos:
    * Implement error handling.
    * Add unique ids.
"""

import subprocess as s
import socket
import selectors
import traceback
import json
from datetime import datetime
from pathlib import Path
from .message import Message
from .message_db import MessageSQL, PostgreWrapperAdmin


def stamp_and_save_json(message, folder='./'):
    """Given a message, stamp the time and save it into a folder.

    Args:
        message (:obj:`message.Message`): Message that will be stored.
        folder (str or :obj:`pathlib.Path`): Path of the folder where the
            message will be stored.
    """

    status_dict = {'retrieved': False,
                   'retrieved_date': '',
                   'recieved': datetime.now().isoformat()}
    dest_folder = Path(folder)
    status_dict['data'] = json.loads(message.content.data)
    filename = status_dict['data']['address']
    filename += status_dict['data']['created'] + '.json'
    dest_filepath = dest_folder / filename
    with open(dest_filepath, 'x') as outfile:
        json.dump(status_dict, outfile, indent=4)
    print(f'Message file saved: {dest_filepath}')


def db_entry_insert(message, psql):
    """Given a message, prepare prepare it to be stored in the given `psql`
    database.

    Args:
        message (:obj:`message.Message`): Message that will be stored.
        psql (:obj:`PostgreWrapperAdmin`): Postgrewrapper to store the message.
    """
    msg_data = json.loads(message.content.data)
    msg = MessageSQL(txid=msg_data['txid'],
                     clientid=msg_data['address'],
                     date_sent=msg_data['created'],
                     date_retrieved=None,
                     keywords=msg_data['keywords'],
                     note=msg_data['note'])
    psql.message_insert(msg)


class RecServer:
    """Server implementation to recieve simple messages.

    Attributes:
        host (str): Host address of the server.
        port (int): Port of the server.
        sel (:obj:`selectors.BaseSelector`): Selector to operate with the
            sockets.
        socket_type (str): Type of the socket that will be used to communicate
            with the server.
        m_path (str or :obj:`pathlib.Path`): Path where the messages will be
            stored.

    Args:
        host (str, optional): Same as attribute. Defaults to None.
        port (int, optional): Same as attribute. Defaults to None
        sel (:obj:`selectors.BaseSelector`, optional): Same as attribute.
            Defaults to None. If None, an :obj:`selectors.BaseSelectors` will
            be created as the selector.
        socket_type (str, optional): Same as attribute. Defaults to None. If
            None, a `sockets.AF_INET` `socket.SOCK_STREAM` will be used.
        m_path (str or :obj:`pathlib.Path`, optional): Same as the attribute.
            Defaults to None.

    Raises:
        NotImplementedError: Wrong selector.


    """
    def __init__(self, host=None, port=None, sel=None,
                 socket_type=None, m_path=None, postgresql=None,
                 notify=False):
        self.host = host
        self.port = port
        self.status = 'down'
        self.m_path = m_path
        self.postgresql = postgresql
        self.notify = notify
        if sel:
            if isinstance(sel, selectors.BaseSelector):
                self.sel = sel
            else:
                msg = 'sel must be a valid selector object.'
                raise NotImplementedError(msg)
        else:
            self.sel = selectors.DefaultSelector()
        if socket_type:
            if isinstance(socket_type, socket.socket):
                self.socket = socket_type
            else:
                msg = 'socket_type must be a valid socket object.'
                raise NotImplementedError(msg)
        else:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def __repr__(self):
        return f'<RecServer: {self.host}:{self.port}>'

    @classmethod
    def from_config(cls, config):
        """Create a :obj:`RecServer` from a config file.

        Args:
            config (:obj:`dict`): Dictionary containing the needed
                configuration.

        Returns:
            :obj:`RecServer` with the given configuration.
        """
        server = cls(**config['server'])
        return server

    def accept(self):
        """Accept a connection"""
        conn, address = self.socket.accept()
        print(f'accepted connection from {address}')
        conn.setblocking(False)
        empty_message = Message(self.sel, conn, address)
        self.sel.register(conn, selectors.EVENT_READ, data=empty_message)

    def listen(self):
        """Listen to the given adress:port"""
        self.socket.bind((self.host, self.port))
        self.socket.listen()
        print(f'listening on {self.host}:{self.port}')
        self.socket.setblocking(False)
        self.sel.register(self.socket, selectors.EVENT_READ, data=None)
        self.status = 'listening'

    def close(self):
        """Detach the socket and close the connection."""
        self.socket.detach()
        self.status = 'down'

    def run(self):
        """Run the server. If the server status is 'down', the `listen` method
        will be executed first. """
        if self.status == 'down':
            self.listen()
        self.status = 'running'
        if self.postgresql:
            print("Connecting to the SQL server...")
            psql = PostgreWrapperAdmin(**self.postgresql)
            print("Checking if the table is stored in the database.")
            psql.table_create()
            print("Success!")
        try:
            while True:
                events = self.sel.select(timeout=None)
                print("Waiting for messages")
                for key, mask in events:
                    if key.data is None:
                        self.accept()
                    else:
                        message = key.data
                        try:
                            message.recieve()
                            message.read_buffer()
                            msg = f'Message from {message.address}: {message.content.data}'
                            stamp_and_save_json(message, self.m_path)
                            if self.postgresql:
                                db_entry_insert(message, psql)
                                print(f'Message stored in {self.postgresql["dbname"]} db.')

                            if self.notify:
                                s.call(['notify-send', msg])
                            message.disconnect()

                        except Exception:
                            print(
                                "main: error: exception for",
                                f"{message.address}:\n{traceback.format_exc()}",
                            )
                            message.disconnect()
        except KeyboardInterrupt:
            print("caught keyboard interrupt, exiting")
        finally:
            self.status = 'listening'
            self.sel.close()
