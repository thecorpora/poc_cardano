#!/usr/bin/env python3
"""Implements the message object that will be sent by the client and readed by
the server.

Todos:
    * Repair the __init__ of the Json subclasses.
    * Better exception handling.
"""
import json
import struct
from warnings import warn


def serialize(ser_obj):
    """Serialize an object into a :obj:`dict` without using _attributes and
    properties.

    Args:
        ser_obj (serializable object): Object that will be transform into a
        dictonary.

    Returns:
        :obj:`dict` containing the data of the serialized object.
    """
    out_dict = {}
    for key, value in ser_obj.__dict__.items():
        if key.startswith('_'):
            continue
        out_dict[key] = value
    return out_dict


def decode_json(json_o, encoding='utf-8'):
    """Decodes a json bytes or string to a :obj:`dict`.

    Args:
        json_o (bytes or str): Json string to be transformed
        encoding (str, optional): In case of json_o in bytes, encoding of the
            bytes.

    Returns:
        :obj:`dict` with the values of the json.
    """
    if isinstance(json_o, bytes):
        json_str = json_o.decode(encoding)
    elif isinstance(json_o, str):
        json_str = json_o
    else:
        msg = 'json must be <bytes> or <str> type'
        raise NotImplementedError(msg)
    return json.loads(json_str)


class PreHeader:
    """2 bytes preheader containing the size of the header.

    Attributes:
        PHEADER_SICE (int): Size of the header length bytestring.
        isize (int): Size of the header, in int.
        bsize (bytes): Size of the header, in bytes.

    Args:
        isize (int, optional): Same as the attribute. Defaults to None.
        bsize (bites, optional): Same as the attribute. Defaults to None.
    """
    PHEADER_SIZE = 2

    def __init__(self, isize=None, bsize=None):
        self.isize = isize
        self.bsize = bsize

    def __repr__(self):
        return f'<({self.isize}) {self.bsize}>'

    def __int__(self):
        return self.isize

    def __bytes__(self):
        return self.bsize

    @classmethod
    def from_int(cls, numb):
        """Create a preheader from an int specifying the size of the header.

        Args:
            numb (int): Size of the header.

        Returns:
            :obj:`PreHeader` containing the specified int.
        """
        out_pheader = cls(isize=numb)
        out_pheader.bsize = out_pheader.struct_pack()
        return out_pheader

    @classmethod
    def from_short(cls, short):
        """Create a preheader from a short specifying the size of the header.

        Args:
            numb (short): Size of the header.

        Returns:
            :obj:`PreHeader` containing the specified short.
        """
        out_pheader = cls(bsize=short)
        out_pheader.isize = out_pheader.struct_unpack()[0]
        return out_pheader

    @classmethod
    def from_jsonheader(cls, json_header):
        """Create a preheader from a header.

        Args:
            numb (:obj:`Json_Header`): Size of the header.

        Returns:
            :obj:`PreHeader` containing the specified short.
        """

        out_pheader = cls(isize=json_header.size)
        out_pheader.bsize = out_pheader.struct_pack()
        return out_pheader

    def struct_pack(self):
        """ Convert the isize attribute to short.

        Returns:
            bytes with the converted value.
        """
        return struct.pack('>H', self.isize)

    def struct_unpack(self):
        """ Convert the bsize attribute to int.

        Returns:
            int with the converted value.
        """
        return struct.unpack('>H', self.bsize)


class JsonBlock:
    """Generic json block class. Implements different methods to work with json
    blocks"""
    def __init__(self):
        self._json_encoded = None

    @classmethod
    def from_json(cls, json_o, encoding='utf-8'):
        """Creates a :obj:`JsonBlock` from a json string.

        Args:
            json_o (bytes or str): Json string or bytes.
            encoding (str, optional): Encoding in case of Json string input in
                bytes. Defaults to utf-8

        Returns:
            :obj:`JsonBlock` with the loaded json string.
        """
        json_r = decode_json(json_o, encoding)
        header = cls(**json_r)
        return header

    @property
    def json_encoded(self):
        """bytes: Encoded Json"""
        if not self._json_encoded:
            self.to_json()
        return self._json_encoded

    @property
    def size(self):
        """int: Size of the encoded block in bytes."""
        return len(self.json_encoded)

    def to_json(self, encoding='utf-8'):
        """Encode the current object into a json bytes.

        Args:
            encoding (str, optional): Encoding of the bytestring.

        Returns:
            bytes containing the serialized class.

        Notes:
            _attributes will not be converted to Json.
        """
        json_s = json.dumps(self,
                            ensure_ascii=False,
                            default=serialize)
        self._json_encoded = json_s.encode(encoding)
        return self._json_encoded

    def to_dict(self):
        """Serialize the current json block into a dictonary.

        Returns:
            :obj:`dict` of the current block.

        Notes:
            _attributes will not be converted to Json.
        """
        return serialize(self)


class JsonHeader(JsonBlock):
    """Json header containing the instruction and the size of the data block.

    Attributes:
        dtype (str): Type of the data encoded. Defaults to None.
        dsize (int): Size of the current header, in chars.
        encoding (str): Encoding of the header.

    Args:
        dtype (str, optional): Same as the attribute. Defaults to None.
        dsize (int, optional): Same as the attribute. Defaults to None
        encoding (str, optional): Same as the attribute. Defaults to 'utf-8'.
    """
    def __init__(self, dtype=None, dsize=None, encoding='utf-8'):
        self.dtype = dtype
        self.dsize = dsize
        self.encoding = encoding
        self._json_encoded = None

    def __repr__(self):
        out_str = f'{self.dtype}'
        out_str += f'[{self.dsize}]'
        out_str += f'({self.encoding})'
        return out_str

    @classmethod
    def from_content(cls, content, encoding='utf-8'):
        """Create a :obj:`JsonHeader` from a :obj:`JsonContent`

        Args:
            content (:obj:`JsonContent`): Json content that will define the
                attributes of the header.
            encoding (str, optional): Encoding of the content file. Defaults to
                'utf-8'.

        Returns:
            :obj:`JsonHeader` with the attributes defined by the content.
        """
        header = cls(dtype='json/instruction',
                     dsize=content.size,
                     encoding=encoding)
        return header


class JsonContent(JsonBlock):
    """Wraper of Json data.

    Attributes:
        instruction (str): Instruction of the block.
        data (str): Data in the block

    Args:
        instruction (str, optional): Same as attribute. Defaults to None.
    """
    def __init__(self, instruction=None, data=None):
        self.instruction = instruction
        if not data:
            self.data = ''
        else:
            self.data = data
        self._json_encoded = None


class Message:
    """Message class to send and retrieve data from the client/server.

    Attributes:
        selector (:obj:`selectors.BaseSelector`): Selector used to operate with
            the socket.
        sack (:obj:`socket.Socket`): Socket that will be used to send and
            recieve the message.
        address ((str, int)): Address of the destinatary of the message. IP and
            port.
        preheader (:obj:`PreHeader`): Preheader of the message containing the
            size of the Header in short.
        header (:obj:`JsonHeader`): Header of the message containing the
            instruction, the size and the encoding of the content.
        content (:obj:`ContentHeader`): Content of the message.

    Args:
        selector (:obj:`selectors.BaseSelector`): Same as attribute.
        sack (:obj:`socket.Socket`): Same as attribute.
        address ((str, int)): Same as attribute.
    """
    def __init__(self, selector, sock, address):
        self.selector = selector
        self.sock = sock
        self.address = address
        self._rbuff = b''
        self._sbuff = b''
        self._header_len = None
        self.preheader = None
        self.header = None
        self.content = None

    def generate_content_auto(self, instruction, data=None):
        """Generate the content, header and preheader for a piece of data.

        Args:
            instruction (str): Instruction of content.
            data (str): Data inside the content
        """
        self.content = JsonContent(instruction, data)
        self.header = JsonHeader.from_content(self.content)
        self.preheader = PreHeader.from_jsonheader(self.header)

    def construct_message_byte(self):
        """Construct the bytestring of the current message using the preheader,
        header and content.

        Returns:
            bytes with the resulting bytestring
        """
        out_str = self.preheader.bsize
        out_str += self.header.json_encoded
        out_str += self.content.json_encoded
        return out_str

    def recieve(self):
        """Recieve the message from the server.

        Raises:
            RuntimeError
        """
        try:
            data = self.sock.recv(4096)
        except BlockingIOError:
            pass
        else:
            if data:
                self._rbuff += data
            else:
                raise RuntimeError('Peer Closed')

    def send(self):
        """Send the message to the server."""
        if self._sbuff:
            piece = repr(self._sbuff)
            print(f'sending {piece} to {self.address}')
            try:
                sent = self.sock.send(self._sbuff)
            except BlockingIOError:
                pass
            else:
                self._sbuff = self._sbuff[sent:]
                if sent and not self._sbuff:
                    self.disconnect()

    def _process_pheader(self):
        preh_s = self._rbuff[:PreHeader.PHEADER_SIZE]
        self.preheader = PreHeader.from_short(preh_s)
        self._rbuff = self._rbuff[PreHeader.PHEADER_SIZE:]

    def _process_header(self):
        json_h_s = self.preheader.isize
        json_h = self._rbuff[:json_h_s]
        self.header = JsonHeader.from_json(json_h)
        self._rbuff = self._rbuff[json_h_s:]

    def _process_content(self):
        json_c_s = self.header.dsize
        json_c = self._rbuff[:json_c_s]
        self.content = JsonContent.from_json(json_c)
        self._rbuff = self._rbuff[json_c_s:]

    def read_buffer(self):
        """Read the preheader, header and content stored in the buffer.

        Raises:
            ValueError: The buffer is empty.
        """
        if not self._rbuff:
            msg = 'recieve buffer is empty'
            raise ValueError(msg)

        self._process_pheader()
        self._process_header()
        self._process_content()

        if self._rbuff:
            warn('Message was larger than specified! skipping.')

    def write_buffer(self):
        """Write the current preahedr, header and content in buffer.
        """
        self._sbuff = self.construct_message_byte()

    def disconnect(self):
        """Close the connection."""
        print(f'disconnecting from {self.address}')
        try:
            self.selector.unregister(self.sock)
        except Exception as unknown:
            msg = "selector.unregister() exception:"
            print(msg, f'{self.address}: {repr(unknown)}')

        try:
            self.sock.close()
        except OSError as unknown:
            msg = "socket.close() exception:"
            print(msg, f'{self.address}: {repr(unknown)}')
