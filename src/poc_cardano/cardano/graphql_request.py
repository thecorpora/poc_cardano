#!/usr/bin/env python3
"""Simple module to perform the queries to the graphQL cardano API.

Attributes:
    DefaultHeaders: Default headers for the queries.

Todos:
    * Error handling.
"""

import json
import requests

DefaultHeaders = {'Content-Type': 'application/json'}


def request_raw_json(txid, graphql_url, query_path, headers=DefaultHeaders):
    """Request json using the given query.

    Args:
        txid (str): Queried transactionid (txid).
        graphql_url (str): Url of the graphql API.
        query_path (str or :obj:`pathlib.Path`): String containing the query.
        headers (:obj:`dict`, optional): Dictionry containing the query
            headers. Defaults to DefaultHeaders.

    Returns:
        :obj:`dict` containing the values from the json obtained from the
        server.
    """
    with open(query_path, 'r', encoding='utf-8') as infile:
        query = json.load(infile)
    query['variables']['id'] = txid
    request = requests.post(graphql_url,
                            data=json.dumps(query),
                            headers=headers)
    request.raise_for_status()
    return request.json()


def get_metadata(txid, graph_url, query_path, headers=DefaultHeaders):
    """Obtain the metadata for a given transaction id.

    Args:
        txid (str): Queried transactionid (txid).
        graphql_url (str): Url of the graphql API.
        query_path (str or :obj:`pathlib.Path`): String containing the query.
        headers (:obj:`dict`, optional): Dictionry containing the query
            headers. Defaults to DefaultHeaders.

    Returns:
        :obj:`dict` containing the values from the metadata in the json
        obtained from the server.
    """
    raw_json = request_raw_json(txid, graph_url, query_path, headers)
    return raw_json['data']['transactions'][0]['metadata'][0]
