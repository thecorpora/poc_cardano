#!/usr/bin/env python3
"""cli_wrapper tries to create a class wrapping the cardano-cli application
provided by Cardano.

Attributes:
    UtxoUnit (namedtuple): :obj:`collections.namedtuple` containing the
        different information contained in a row of a raw UTXO Table.

Todo:
    * Add error handling for the operations in this module.
    * Create a class to wrap the cardano-cli arguments.
"""

import subprocess
from collections import namedtuple

UtxoUnit = namedtuple('UtxoUnit', ['TxHash', 'TxIx', 'amount', 'currency'])


class UtxoTable:
    """Class to manipulate the UTXO table retrieved from cardano-cli

    Attributes:
        rows (obj:`list` of :obj:`UtxoUnit`): List containing the different
            UtxoUnits found in the raw UTXO Table.
    """

    def __init__(self):
        self.rows = []

    def __len__(self):
        return len(self.rows)

    def __getitem__(self, idx):
        return self.rows[idx]

    def _calc_amount(self):
        total = 0
        for item in self.rows:
            total += item.amount
        return total

    @property
    def total_amount(self):
        """int: Total amount of available ADA in the current :obj:`UtxoTable`
        """
        return self._calc_amount

    @classmethod
    def from_raw(cls, raw_table):
        """Parses the UTXO table string retrieved using the cardano-cli
        application.

        Args:
            raw_table (str): String obtained from cardano-cli.

        Returns:
            :obj:`UtxoTable` object from the parsed string.
        """
        utxo_out = cls()
        utxo_rows = raw_table.strip().splitlines()
        for row in utxo_rows[2:]:
            tx_hash, tx_ix, amount, currency = row.split()[:4]
            utxo_out.rows.append(UtxoUnit(TxHash=tx_hash.decode('utf-8'),
                                          TxIx=tx_ix.decode('utf-8'),
                                          amount=int(amount),
                                          currency=currency.decode('utf-8')))
        return utxo_out


class SingleTransaction:
    """Class to define a single ADA transaction.

    Attributes:
        fee (int): Fee of the transaction.
        metadata (str or :obj:`pathlib.Path`): Path to the metadata file that
            will be uploaded in the transaction.
        draft_path (str or :obj:`pathlib.Path`): Path to the draft transaction
            file.
        magic (int): Magic number associated with the Cardano network.
        sign_path (str or :obj:`pathlib.Path`): Path to the signed transaction
            file.
        protocol (str or :obj:`pathlib.Path`): Path to the protocol file used.
        transactions (:obj:`list` of :obj:`tuple` of (str, int)): Contains the
            transactions that will be made after the operation.
        utxos (:obj:`list` of :obj:`tuple` of :obj:`UtxoUnit`): Contains the
            UTXO that will be used in the transaction.
        txid (str): Transaction id (txid) of the transaction.

    Arguments:
        metadata (str or :obj:`pathlib.Path`, optional): Description in
            attribute. Defaults to None.
        draft_path (str or :obj:`pathlib.Path`, optional): Description in
            attribute. Defaults to None.
        magic (int, optional): Description in attribute. Defaults to None
        protocol (str or :obj:`pathlib.Path`, optional): Description in
            attribute. Defaults to None.
        fee (int, optional): Description in attribute. Defaults to None.
        sign_path (str or :obj:`pathlib.Path`, optional): Description in
            attribute. Defaults to None.
    """
    def __init__(self, metadata=None, draft_path=None, magic=None,
                 protocol=None, fee=None, sign_path=None):
        self.fee = fee
        self.metadata = metadata
        self.draft_path = draft_path
        self.magic = magic
        self.sign_path = sign_path
        self.protocol = protocol
        self.transactions = []
        self.utxos = []
        self.txid = None

    def __str__(self):
        return ' '.join(self.build_raw())

    @property
    def in_count(self):
        """int: Number of UTXOs involved in this transaction."""
        return len(self.utxos)

    @property
    def out_count(self):
        """int: Number of transactions"""
        return len(self.transactions)

    def utxo_add(self, utxo_unit):
        """Adds an UTXO to the transaction.

        Args:
            utxo_unit (:obj:`UtxoUnit`): Unit that will be added to the
                transaction.
        """
        self.utxos.append(utxo_unit)

    def transaction_add(self, address, amount, pay_fee=False):
        """Adds a UTXO transaction to the transaction object.

        Args:
            address (str): Address of the wallet involved.
            amount (int): Number of lovelaces transferred.
            pay_fee (bool): If true, the fees of the transaction will be taken
                from the sepecified wallet.
        """
        if pay_fee:
            amount -= self.fee
        self.transactions.append((address, amount))

    def build_raw(self):
        """Builds the arguments needed for cardano-cli to create a draft
        transaction.

        Returns:
            :obj:`list` of str containing the arguments that will be passed to
            cardano-cli.
        """
        t_arguments = ['transaction', 'build-raw',
                       '--fee', str(self.fee),
                       '--out-file', self.draft_path]
        for utxo in self.utxos:
            t_arguments += ['--tx-in', '{}#{}'.format(utxo.TxHash, utxo.TxIx)]
        for transaction in self.transactions:
            t_arguments += ['--tx-out', '{}+{}'.format(*transaction)]
        if self.metadata:
            t_arguments += ['--metadata-json-file', self.metadata]
        return t_arguments

    def build_calculate_min_fee(self):
        """Builds the arguments needed for cardano-cli to calculate the minimum
        transaction fee.

        Returns:
            :obj:`list` of str containing the arguments that will be passed to
            cardano-cli.
        """
        t_arguments = ['transaction', 'calculate-min-fee',
                       '--tx-body-file', self.draft_path,
                       '--tx-in-count', str(self.in_count),
                       '--tx-out-count', str(self.out_count),
                       '--witness-count', str(1),
                       '--testnet-magic', str(self.magic),
                       '--protocol-params-file', str(self.protocol)]
        return t_arguments

    def build_sign(self, key_path):
        """Builds the arguments needed for cardano-cli to sign the transaction.

        Args:
            key_path (str or :obj:`pathlib.Path`): Path the file containing the
                private key that will be used to sign the transaction.

        Returns:
            :obj:`list` of str containing the arguments that will be passed to
            cardano-cli.
        """
        t_arguments = ['transaction', 'sign',
                       '--tx-body-file', str(self.draft_path),
                       '--signing-key-file', str(key_path),
                       '--testnet-magic', str(self.magic),
                       '--out-file', str(self.sign_path)]
        return t_arguments

    def build_submit(self):
        """Builds the arguments needed for cardano-cli to submit a signed
        transaction.

        Returns:
            :obj:`list` of str containing the arguments that will be passed to
            cardano-cli.
        """

        t_arguments = ['transaction', 'submit',
                       '--tx-file', str(self.sign_path),
                       '--testnet-magic', str(self.magic)]
        return t_arguments

    def build_check_txid(self):
        """Builds the arguments needed for cardano-cli to check the transaction
        id of the signed transaction.

        Returns:
            :obj:`list` of str containing the arguments that will be passed to
            cardano-cli.
        """

        t_arguments = ['transaction', 'txid',
                       '--tx-file', str(self.sign_path)]
        return t_arguments

    def clear_transactions(self):
        """Removes all the transactions stored iin the transactions
        attributed
        """
        self.transactions = []

    def update_fee(self, fee):
        """Update the fee attribute with the given fee AND apply the fee the
        first transaction stored in the transactions attribute.

        Args:
            fee (int): Cost of the fee, in lovelaces.
        """
        self.fee = fee
        f_transaction = self.transactions[0]
        new_amount = f_transaction[1] - fee


class CardanoCli:
    """Defines the environment of the cardano-cli to execute different commands.

    Attributes:
        paths (:obj:`dict` of :obj:`pathlib.Path`): Paths needed for
            cardano-cli to run different commands:
                'cardano-cli': Path to the cardano executable.
                't_draft': Path to the draft transaction.
                'protocol': Path to the protocol file.
                'metadata': Path to the metadata file.
                't_signed': Path to the signed transaction.
                'private_key': Path to the file containing the private key.
        magic (int): Magic number associated with the Cardano network.
        w_address (str): Address of the wallet that will be used.
        utxo_table (:obj:`UtxoTable`): UtxoTable of the current wallet.
    Args:
        paths (:obj:`dict` of :obj:`pathlib.Path`, optional): Description in
            attribute. Defaults to None.
        magic (int, optional): Description in attribute. Defaults to None.
        w_address (str, optional): Description in attribute. Defaults to None.
    """
    def __init__(self, paths=None, magic=None, w_address=None):
        self.paths = paths
        self.magic = magic
        self.w_address = w_address
        self.utxo_table = None

    @classmethod
    def from_config(cls, config):
        """Creates a :obj:`CardanoCli` with the given configuration.

        Args:
            config (:obj`dict`): Dictionary with the environment configuration
                More details in the class attributes description.
        """
        wrapper = cls(**config['cardano'])
        return wrapper

    @property
    def cli(self):
        """str or :obj:`pathlib.Path`: Path to the cardano-cli executable"""
        return self.paths['cardano-cli']

    def _run(self, t_arguments):
        return subprocess.check_output([self.cli, *t_arguments])

    def _draft_transaction(self):
        draft_transaction = SingleTransaction(fee=0)
        draft_transaction.draft_path = self.paths['t_draft']
        draft_transaction.metadata = self.paths['metadata']
        draft_transaction.magic = self.magic
        draft_transaction.protocol = self.paths['protocol']
        return draft_transaction

    def _metadata_draft_transaction(self, utxo_units=None,
                                    get_utxo_table=True):
        query_transaction = self._draft_transaction()
        if utxo_units:
            for utxo in utxo_units:
                query_transaction.utxo_add(utxo)
        elif get_utxo_table:
            if not self.utxo_table:
                self.get_utxo_table()
            query_transaction.utxo_add(self.utxo_table.rows[0])
        query_transaction.transaction_add(self.w_address, amount=0,
                                          pay_fee=False)
        return query_transaction

    def _metadata_update_draft_transaction(self, transaction, fee):
        transaction.fee = fee
        new_amount = transaction.utxos[0].amount
        transaction.clear_transactions()
        transaction.transaction_add(self.w_address, new_amount, pay_fee=True)
        return transaction

    def query_utxo(self):
        """Query the UTXO table of the current wallet.

        Returns:
            str with the raw table.
        """
        t_arguments = ['query', 'utxo',
                       '--testnet-magic', str(self.magic),
                       '--address', self.w_address]
        raw_utxo_table = self._run(t_arguments)
        return raw_utxo_table

    def query_protocol_parameters(self):
        """Query for the protocol parameters of the current network
        and save it in the paths['protocol'] paths.
        """
        t_arguments = ['query', 'protocol-parameters',
                       '--testnet-magic', str(self.magic),
                       '--out-file', self.paths['protocol']]
        self._run(t_arguments)

    def get_utxo_table(self):
        """Query and update the utxo_table attribute with the current
        UTXO table associated to the given wallet.
        """
        self.utxo_table = UtxoTable.from_raw(self.query_utxo())

    def run_transaction(self, transaction):
        """Run the given transaction with the specified cardano-cli
        executable

        Args:
            transaction (:obj:`SingleTransaction`): Transaction to run.

        Returns:
            Updated :obj:`SingleTransaction`.
        """
        t_arguments = transaction.build_raw()
        transaction_out = self._run(t_arguments)
        return transaction_out

    def run_calc_min_fee(self, transaction):
        """Calculate fee for the given transaction.

        Args:
            transaction (:obj:`SingleTransaction`): Transaction to calculate
                the fee.

        Returns:
            int with the calculated fee, in lovelaces.
        """
        t_arguments = transaction.build_calculate_min_fee()
        calculated_fee = self._run(t_arguments)
        return int(calculated_fee.strip().split()[0])

    def run_sign(self, transaction):
        """Calculate fee for the given transaction.

        Args:
            transaction (:obj:`SingleTransaction`): Transaction to calculate
                the fee.

        Returns:
            int with the calculated fee, in lovelaces.
        """
        transaction.sign_path = self.paths['t_signed']
        t_arguments = transaction.build_sign(self.paths['private_key'])
        sign_out = self._run(t_arguments)
        transaction.txid = self.run_get_id(transaction)
        return sign_out

    def run_get_id(self, transaction):
        """Retrieve the transaction id (txid) of the given transaction.

        Args:
            transaction (:obj:`SingleTransaction`): Transaction from which
                the id will be retrieved.

        Returns:
            str with the txid
        """
        t_arguments = transaction.build_check_txid()
        txid = self._run(t_arguments).strip().decode('utf-8')
        return txid

    def run_submit(self, transaction):
        """Run the given signed transaction with the specified cardano-cli
        executable to submit the transaction.

        Args:
            transaction (:obj:`SingleTransaction`): Transaction to run.

        Returns:
            Updated :obj:`SingleTransaction`.
        """
        t_arguments = transaction.build_submit()
        submit_out = self._run(t_arguments)
        return submit_out
