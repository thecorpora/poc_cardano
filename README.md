
# Table of Contents

1.  [Installation](#orge9c53cf)
    1.  [Dependencies](#orgc29a606)
        1.  [Python](#org24edd3c)
        2.  [Cardano](#org71303a5)
        3.  [PostgreSQL](#org2490f18)
    2.  [Automatic Install](#org3e640cb)
    3.  [Installing poc-cardano in Raspbian 11 Bullseye](#org7c0ca50)
2.  [Use](#orgc8caffc)
    1.  [Configuration](#org69f9a4a)
        1.  [Location](#org72f81ae)
        2.  [Parameters](#org416b68e)
        3.  [Notes](#org6c32232)
    2.  [Basic poc\_cardano run](#orgb8878d1)
    3.  [Execution Arguments](#orgfe68832)
3.  [Scheme](#org76d1265)
4.  [Author](#org7309877)



<a id="orge9c53cf"></a>

# Installation


<a id="orgc29a606"></a>

## Dependencies


<a id="org24edd3c"></a>

### Python

This piece of software is written in Python 3. A Python 3 interpret is needed to
execute the code.

1.  Additional Packages

    This software rely in two external libraries:
    ([pycrpytodome](https://pypi.org/project/pycryptodome/)) to implement the cryptographic operations found in the `crypto`
    module and ([requests](https://docs.python-requests.org/en/master/index.html)) to implement [graphQL](https://github.com/input-output-hk/cardano-graphql) queries to [Cardano Explorer](https://explorer.cardano-testnet.iohkdev.io/en.html).
    `message_db` module uses [psycopg3](https://www.psycopg.org/), a strongly typed PostgreSQL API, to increase
    the security while interacting with databases.
    
    The easiest way to install `pycryptodome`, `requests` and `psycopg` is by using [pip](https://docs.python.org/3/installing/index.html):
    
        pip install pycryptodome requests psycopg
    
    In some cases Python 3 has its own pip distribution named as `pip3`:
    
        pip3 install pycryptodome requests psycopg
    
    `pip` should automatically deal with the needed dependencies.


<a id="org71303a5"></a>

### Cardano

poc-cardano communicates with the Cardano network using `cardano-cli`. A running
instance of `cardano-node` and `cardano-cli` is needed to upload the messages to
the Cardano Blockchain.

1.  cardano-node

    `cardano-node` can be compiled from scratch using `ghc` and `cabal`. A tutorial
    can be found [here](https://developers.cardano.org/docs/get-started/installing-cardano-node/). Arch User Repository (AUR) also hosts a recipe for [cardano-node](https://aur.archlinux.org/packages/cardano-node/).
    
    However, `cabal` uses [hackage](https://hackage.haskell.org/) as its main repository and a newer version of
    `ghc` will be unable to solve the needed dependencies. For these cases, [nix](https://nixos.org/) can
    be used to compile the cardano node. A tutorial can be found [here](https://github.com/input-output-hk/cardano-node/blob/master/doc/getting-started/building-the-node-using-nix.md).
    
    Additionally, a [docker](https://www.docker.com/) container of `cardano-node` can be found [here](https://hub.docker.com/r/inputoutput/cardano-node). I
    strongly recommend reading [this tutorial](https://docs.cardano.org/getting-started/installing-the-cardano-node) to learn how to run `cardano-node` and
    `cardano-cli` using the docker container.

2.  cardano-cli

    `cardano-cli` should be compiled during the building of `cardano-node`. The
    `docker` container of [cardano-node](https://hub.docker.com/r/inputoutput/cardano-node) also contains a working build of
    `cardano-cli`.
    
    The `cardano-cli` binary location must be specified in the configuration file.


<a id="org2490f18"></a>

### PostgreSQL

A running `postgresql` database is required to store and retrieve transaction
messages. `postgresql` can be found in almost all the common Linux
distributions. A [tutorial](https://www.postgresql.org/docs/current/tutorial.html) to set up a database can be found in its [official
documentation](https://www.postgresql.org/docs/).


<a id="org3e640cb"></a>

## Automatic Install

An automatic install of `poc_cardano` is available through the use of `pip`.
Python dependencies will be automatically solved using this method. Manual
installation of `cardano-cli` and a running instance of `cardano-node` are still
required.

Before starting automatic install ensure that the default configuration is
modified in `/poc_cardano/src/poc_cardano/config/config.yaml`.

To install using `pip`:

    git clone https://gitlab.com/thecorpora/poc_cardano.git
    cd poc_cardano
    pip install


<a id="org7c0ca50"></a>

## Installing poc-cardano in Raspbian 11 Bullseye

The easiest way to install poc-cardano in Raspbian 11 is through the use of a
[Python Virtual Enviroment library](https://docs.python.org/3/library/venv.html) (`venv`). It is possible to download the
`venv` library from the raspbian repository:

    sudo apt install python3-venv

Then, a virtual environment in the home `dir` of the current user can be created
and activated:

    python3 -m venv "$HOME/cardano_env"
    source "$HOME"/cardano_env/bin/activate

Once activated, upgrade the version of both `setuptools` and `pip` to be able to
install the `poc-cardano` package:

    pip install --upgrade setuptools pip

With the environment activated follow the steps found in [1.2](#org3e640cb).
Finally, to deactivate the environment use:

    deactivate


<a id="orgc8caffc"></a>

# Use


<a id="org69f9a4a"></a>

## Configuration


<a id="org72f81ae"></a>

### Location

1.  Default Configuration File

    `poc_cardano` uses a `.yaml` configuration file to search for the paths of the
    Cardano executables, temp files and different Cardano network parameters.
    
    By default, `poc_cardano` will look for the configuration file stored in
    `./src/poc_cardano/config/config.yaml`. Then, if the user wants to change the
    default paremeters this, the `config.yaml` file needs to be modified with the
    needed paremeters before the `pip` install.

2.  Home Configuration File

    `poc_cardano` will search for the `$HOME/poc_cardanorc` configuration file
    before using the default configuration file saved during the pip install.

3.  Specifying Configuration File Manually

    The configuration file can also be specified as optional argument when calling
    `poc_cardano`:
    
        python -m poc_cardano --config "config_file.yaml" server


<a id="org416b68e"></a>

### Parameters

The parameters that can be specified in the configuration file are the
following:

-   **server:** Configuration related with the `poc-cardano` server.
    -   **host:** Host address of the server (e.g. 127.0.0.1 for localhost)
    -   **port:** Port of the server.
    -   **notify:** Wether or not send a when a message is received. Requires
        `libnotify`.
    -   **postgresql:** Configuration of the postgresql server.
        -   **dbname:** Name of the database to use.
        -   **user:** Username to log in the database.
        -   **password:** Password to log in the database. If the user requires no
            password, set it to null.
        -   **host:** Host address to the `postgresql` database.
        -   **port:** Port of the `postgresql` database.
-   **images:** Configuration related to the metadata images.
    -   **path:** Path where the images will be downloaded.
-   **cardano:** Configuration related to the cardano utils.
    -   **paths:** Paths to different cardano components.
        -   **cardano-cli:** Path to `cardano-cli` binary.
        -   **private\_key:** Path to your wallet private key.
        -   **public\_key:** Path to your wallet public key.
        -   **t\_draft:** Path where your transaction draft will be stored.
        -   **t\_signed:** Path where your signed transaction will be stored.
        -   **protocol:** Path to the `protocol.json` cardano file. Additional
            information about the file can be found in [Cardano Developers Portal](https://developers.cardano.org/docs/stake-pool-course/handbook/create-simple-transaction/).
        -   **metadata:** When generating the transaction, file where the metadata file
            will be stored.
        -   **metadata\_tmp:** File where temporary metadata will be generated. If using
            a compiled cardano-cli binary instead of the docker distribution,
            `metadata` and `metadata_tmp` should be the same value.
    -   **w\_adress:** Wallet address.
    -   **magic:** Magic number of the test network.
-   **graphql:** Options for the `graphql` to retrieve images from the `poc-cardano`
    -   **url:** Url to the cardano faucet graphql.
    -   **query:** Path to the `.json` file containing the query to retrieve the
        transaction data. Relative paths will take the default `poc-cardano` dir as
        the root. If a custom query is required, provide the full path.


<a id="org6c32232"></a>

### Notes

Notice that a partial configuration file is not allowed. To customize your
configuration it is highly recommended to use the [config.yaml](src/poc_cardano/config/config.yaml) that can be found
in this repository.


<a id="orgb8878d1"></a>

## Basic poc\_cardano run

If the package was installed via `pip`, you can execute `poc_cardano` using the
following command:

    python -m poc_cardano


<a id="orgfe68832"></a>

## Execution Arguments

Three different procedures are available for execution:

-   **`server`:** Runs the msg-server and listen for requests.
-   **`send`:** Send an image-msg to a listening server.
-   **`retrieve`:** Download an image from the testnet cardano blockchain using a
    received transaction id.
-   **`query`:** Retrieve matching messages from the `postgresql` database.

Use `--help` to obtain the different arguments that can be used for each
procedure:

    python -m poc_cardano send --help

Note that `send` will require a running instance of `cardano-node` and an
available `cardano-cli` binary file. `query` will require an available running
postgresql database.


<a id="org76d1265"></a>

# Scheme

![img](images/scheme.svg)


<a id="org7309877"></a>

# Author

-   Sergio Pablo-Garcia Carrillo ([Google Scholar](https://scholar.google.com/citations?user=r-C-tg4AAAAJ))

